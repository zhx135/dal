import sqlite3

# account name, id, where graduate, the place of school, what occupation, the place of his work, email, phone

databasename = ''

# Connect to database:
db = sqlite3.connect(databasename)
cursor = db.cursor()

db.commit()

# Setters:

def addAlumni(alumid, schoolid, jobid, comid, name, gradyear):
    cursor.execute('''INSERT INTO Alumni(AlumID, SchoolID, JobID, CompanyID, Name, GradYear)
                  VALUES(:aid, :sid, :jid, :cid, :name, :gy)''',
                  {'aid' : alumid, 'sid': schoolid, 'jid': jobid, 'cid': comid, 'name': name, 'gy': gradyear})
    db.commit()

def addSchool(schoolid, schoolName):
    cursor.execute('''INSERT INTO School(SchoolID, SchoolName) VALUES(:sid, :sname)''', {'sid': schoolid, 'sname':schoolName})
    db.commit()

def addJob(jobid, jobname):
    cursor.execute('''INSERT INTO Job(JobID, JobName) VALUES(:jid, :jname)''', {'jid':jobid, 'jname':jobname})
    db.commit()

def addCom(comid, comname):
    cursor.execute('''INSERT INTO Company(CompanyID, CompanyName) VALUES(:cid, :cname)''', {'cid':comid,'cname':comname})
    db.commit()



# Updaters:

def updateAlumniSchool(alumid, schoolid):
    cursor.execute('''UPDATE Alumni set SchoolID = ? where AlumID = ?''',
                   (schoolid, alumid))
    db.commit()

def updatAlumniGradyear(alumid, year):
    cursor.execute('''UPDATE Alumni set GradYear = ? where AlumID = ?''',
                   (year, alumid))
    db.commit()

def updateAlumniJob(alumid, jobid):
    cursor.execute('''UPDATE Alumni set JobID = ? where AlumID = ? ''',
                   (jobid, alumid))
    db.commit()

def updateAlumniCom(alumid, comid):
    cursor.execute('''UPDATE Alumni set CompanyID = ? where AlumID = ?''',
                   (comid, alumid))
    db.commit()

def updateAlumniName(alumid, name):
    cursor.execute('''UPDATE Alumni set Name = ? where AlumID = ?''',
                   (name, alumid))
    db.commit()




# Getters:

# Get SchoolID from table School by giving the school name
def getSchoolID(school):
    cursor.execute('''SELECT SchoolID FROM School WHERE SchoolName = ?''', (school))
    for row in cursor:
        schoolid = row[0]
    return schoolid

# Get SchoolName from table School by giving the schoolID
def getSchoolName(schoolid):
    cursor.execute('''SELECT SchoolName FROM School WHERE SchoolID = ?''', (schoolid))
    for row in cursor:
        schoolname = row[0]
    return schoolname

# Get JobID from table Job by giving the job name
def getJobID(job):
    cursor.execute('''SELECT JobID FROM Job WHERE JobName = ?''', (job))
    for row in cursor:
        jobid = row[0]
    return jobid

# Get JobName from table Job by giving the jobID
def getJobName(jobid):
    cursor.execute('''SELECT JobName FROM Job WHERE JobID = ?''', (jobid))
    for row in cursor:
        job = row[0]
    return job

# Get CompanyID from table Company by giving the company name
def getComID(company):
    cursor.execute('''SELECT CompanyID FROM Company WHERE CompanyName = ?''', (company))
    for row in cursor:
        comID = row[0]
    return comID

# Get CompanyName from table Company by giving the companyID
def getComName(comid):
    cursor.execute('''SELECT CompanyName FROM Company WHERE CompanyID = ?''', (comid))
    for row in cursor:
        com = row[0]
    return com

# Get an alumnus from table Alumni by giving the alumid
def getAlumniByID(alumid):
    cursor.execute('''SELECT * FROM Alumni WHERE AlumID = ?''', (alumid))
    alumni = []
    for row in cursor:
        alumni.append(row)
    return alumni

# Get an alumnus from table Alumni by giving the name
def getAlumniByName(alumname):
    cursor.execute('''SELECT * FROM Alumni WHERE Name = ?''', (alumname))
    alumni = []
    for row in cursor:
        alumni.append(row)
    return alumni

# Get an alumnus from table Alumni by giving the schoolID
def getAlumniBySchool(schoolid):
    cursor.execute('''SELECT * FROM Alumni WHERE SchoolID = ?''', (schoolid))
    list_alumni = []
    for row in cursor:
        list_alumni.append(row)
    return list_alumni

# Get an alumnus from table Alumni by giving the graduation year
def getAlumniFromYear(year):
    cursor.execute('''SELECT * FROM Alumni WHERE GradYear = ?''', (year))
    list_alumni = []
    for row in cursor:
        list_alumni.append(row)
    return list_alumni

# Get an alumnus from table Alumni by giving the jobID
def getAlumniFromJob(jobid):
    cursor.execute('''SELECT * FROM Alumni WHERE JobID = ?''', (jobid))
    list_alumni = []
    for row in cursor:
        list_alumni.append(row)
    return list_alumni

# Get an alumnus from table Alumni by giving the companyID
def getAlumniFromCom(comid):
    cursor.execute('''SELECT * FROM Alumni WHERE CompanyID = ?''', (comid))
    list_alumni = []
    for row in cursor:
        list_alumni.append(row)
    return list_alumni

# Get the schoolID from table Alumni by giving the alumid
def getSchoolOfAlumni(alumid):
    cursor.execute('''SELECT SchoolID FROM Alumni WHERE AlumID = ?''', (alumid))
    sid = -1
    for row in cursor:
        sid =  row[0]
    return sid

# Get JobID from table Alumni by giving the alumid
def getJobOfAlumni(alumid):
    cursor.execute('''SELECT JobID FROM Alumni WHERE AlumID = ?''', (alumid))
    jid = -1
    for row in cursor:
        jid = row[0]
    return jid

# Get ComID from table Alumni by giving the alumid
def getCompanyOfAlumni(alumid):
    cursor.execute('''SELECT CompanyID FROM Alumni WHERE AlumID = ?''', (alumid))
    cid = -1
    for row in cursor:
        cid =  row[0]
    return cid

# Get the graduation year from table Alumni by giving the alumid
def getGYOfAlumni(alumid):
    cursor.execute('''SELECT GradYear FROM Alumni WHERE AlumID = ?''', (alumid))
    year = -1
    for row in cursor:
        year = row[0]
    return year

# Delete an alumni
def deleteAlumni(alumid):
    cursor.execute('''DELETE from Alumni where AlumID = ?''', (alumid))
    db.commit()
